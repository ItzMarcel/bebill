"use strict";

document.addEventListener("DOMContentLoaded", init);

function init(){
	addListeners();
}

function addListeners(){
	document.querySelector("#next").addEventListener("click", checkForm);
	document.querySelector("form").addEventListener("submit", checkForm);
}

function checkForm(e){
	e.preventDefault();

	let personalized = checkGeneratorType();

	if (personalized){
		generatePersonalized();
	}
}

function generatePersonalized(){
	let fields = getFields();
	let url = `http://belikebill.azurewebsites.net/billgen-API.php?default=1`;

	if (fields.text !== "") {
		url = `http://belikebill.azurewebsites.net/billgen-API.php?text=${fields.text}`;
		showImage(url);
	} else if (fields.name !== "") {
		url = `http://belikebill.azurewebsites.net/billgen-API.php?default=1&name=${fields.name}&sex=${fields.gender}`;
		showImage(url);
	}
}

function showImage(url){
	getData(url);
	document.querySelector("form").style.display = "none";
}

function checkGeneratorType(){
	let checkedRadio = document.querySelector("input[type='radio']:checked").value;

	if (checkedRadio === "normal"){

		let url = `http://belikebill.azurewebsites.net/billgen-API.php?default=1`;
		showImage(url);
		return false;

	} else if (checkedRadio === "personalized" && name === ""){
		document.querySelector("fieldset.start-fieldset").style.display = "none";
		document.querySelector("fieldset.personalized-fieldset").style.display = "block";
		return true;
	}

	return false;

}

function getFields(){
	let name = document.querySelector("#name").value;
	let gender = document.querySelector(".personalized-fieldset > input[type='radio']:checked").value;
	let text = purifyText(document.querySelector("#text").value);

	return {'name': name,
					'gender': gender,
					'text': text
				};
}

function purifyText(str){
	str = str.replace(/(?:\r\n|\r|\n)/g, "%0D%0A");
	str = str.replace(" ", "%20");
	return str;
}

function generateImg(url){
	let img = document.createElement("img");
	img.setAttribute("title", "be like bill");
	img.setAttribute("alt", "be like bill");
	img.src = url;
	document.querySelector(".imageholder").appendChild(img);
}

function getData(url){
	fetch(url, {
		method: 'get'
	}).then(function(response){
		return response;
	}).then(function(response){
		generateImg(response.url);
	}).catch(function(error){
		//throw error
	});
}
